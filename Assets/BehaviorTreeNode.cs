using System;

namespace MoleGames.BehaviorTree
{
    [Serializable]
    public abstract class BehaviorTreeNode
    {
        abstract public ExecutableNodeState Evaluate();
    }

    public enum ExecutableNodeState
    {
        Invalid = default,
        Succeeded,
        Failed,
    }
}