using System;

namespace MoleGames.BehaviorTree
{
    public sealed class DebugCallbackBTN : ActionBTN
    {
        readonly Action callback;
        readonly string message;

        public DebugCallbackBTN(Action callback, string message)
        {
            this.callback = callback ?? delegate { UnityEngine.Debug.LogWarning("Unassigned callback Behavior Tree Node"); };
            this.message = message ?? "Undefined debugging action node evaluated";
        }

        public override ExecutableNodeState Evaluate()
        {
            UnityEngine.Debug.Log(message);
            callback();
            return ExecutableNodeState.Succeeded;
        }
    }
}