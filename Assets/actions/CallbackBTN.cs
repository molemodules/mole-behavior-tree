using System;

namespace MoleGames.BehaviorTree
{
    public sealed class CallbackBTN : ActionBTN
    {
        readonly Action callback;

        public CallbackBTN(Action callback)
        {
            this.callback = callback ?? delegate { UnityEngine.Debug.LogWarning("Unassigned callback Behavior Tree Node"); };
        }

        public override ExecutableNodeState Evaluate()
        {
            callback();
            return ExecutableNodeState.Succeeded;
        }
    }
}