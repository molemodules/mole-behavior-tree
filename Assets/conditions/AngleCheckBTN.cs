using System;
using UnityEngine;

namespace MoleGames.BehaviorTree
{
    public sealed class AngleCheckBTN : ConditionBTN
    {
        readonly Func<Transform> agentProvider;
        readonly Func<Vector3> targetProvider;
        readonly Func<float> thresholdProvider;

        Transform Agent => agentProvider();
        Vector3 Target => targetProvider();
        float Threshold => thresholdProvider();

        protected override Func<bool> Predicate => delegate { return Vector3.Angle(Agent.forward, Target - Agent.position) < Threshold; };

        public AngleCheckBTN(
            BehaviorTreeNode child,
            Func<float> threshold,
            Func<Transform> agent,
            Func<Vector3> target)
            : base(child)
        {
            thresholdProvider = threshold;
            agentProvider = agent;
            targetProvider = target;
        }
    }
}