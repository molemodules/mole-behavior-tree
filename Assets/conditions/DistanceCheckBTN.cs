using System;
using UnityEngine;

namespace MoleGames.BehaviorTree
{
    public sealed class DistanceCheckBTN : ConditionBTN
    {
        readonly Func<OverlapType> overlapProvider;
        readonly Func<Vector3> firstPositionProvider;
        readonly Func<Vector3> secondPositionProvider;
        readonly Func<float> thresholdProvider;

        Vector3 FirstPosition => firstPositionProvider();
        Vector3 SecondPosition => secondPositionProvider();
        float Threshold => thresholdProvider();
        OverlapType Overlap => overlapProvider();
        protected override Func<bool> Predicate => EvaluatePositions;

        public DistanceCheckBTN(
            BehaviorTreeNode child,
            Func<float> threshold,
            Func<Vector3> first,
            Func<Vector3> second,
            Func<OverlapType> overlap)
            : base( child)
        {
            thresholdProvider = threshold;
            firstPositionProvider = first;
            secondPositionProvider = second;
            overlapProvider = overlap;
        }

        bool EvaluatePositions()
        {
            return Overlap switch
            {
                OverlapType.Inside => !outside(),
                OverlapType.Outside => outside(),
                _ => throw new NotImplementedException(),
            };

            bool outside() => Vector3.Distance(FirstPosition, SecondPosition) > Threshold;
        }

    }

    public enum OverlapType
    {
        Invalid = default,
        Inside,
        Outside,
    }
}