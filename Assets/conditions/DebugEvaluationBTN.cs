using System;

namespace MoleGames.BehaviorTree
{
    public sealed class DebugEvaluationBTN : ConditionBTN
    {
        readonly Func<bool> condition;
        readonly string message;

        protected override Func<bool> Predicate
        {
            get
            {
                UnityEngine.Debug.Log(message);
                return condition;
            }
        }

        public DebugEvaluationBTN(
            BehaviorTreeNode child,
            Func<bool> condition,
            string message)
            : base(child)
        {
            this.condition = condition;
            this.message = message ?? "Undefined debugging condition node evaluated";
        }
    }
}