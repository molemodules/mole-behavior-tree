using System;

namespace MoleGames.BehaviorTree
{
    public sealed class ThresholdConditionBTN : ConditionBTN
    {
        readonly Func<float> current;
        readonly Func<float> threshold;

        float Current => current();
        float Threshold => threshold();
        protected override Func<bool> Predicate => delegate { return Current > Threshold; };

        public ThresholdConditionBTN(
            BehaviorTreeNode child,
            Func<float> current,
            Func<float> threshold)
            : base(child)
        {
            this.current = current;
            this.threshold = threshold;
        }
    }
}