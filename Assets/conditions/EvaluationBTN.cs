using System;

namespace MoleGames.BehaviorTree
{
    public sealed class EvaluationBTN : ConditionBTN
    {
        readonly Func<bool> condition;

        protected override Func<bool> Predicate => condition;

        public EvaluationBTN(
            BehaviorTreeNode child,
            Func<bool> condition)
            : base(child)
        {
           this.condition = condition;
        }
    }
}