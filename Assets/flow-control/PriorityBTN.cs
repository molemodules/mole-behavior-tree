using System.Collections.Generic;
using System.Linq;

namespace MoleGames.BehaviorTree
{
    public sealed class PriorityBTN : FlowControlBTN
    {
        readonly BehaviorTreeNode[] childNodes;

        public PriorityBTN(IEnumerable<BehaviorTreeNode> childNodes)
        {
            this.childNodes = childNodes.ToArray();
        }

        public override ExecutableNodeState Evaluate()
        {
            ExecutableNodeState state = ExecutableNodeState.Failed;
            int index = 0;
            while (index < childNodes.Length && state == ExecutableNodeState.Failed)
            {
                state = childNodes[index].Evaluate();
                index++;
            }

            return state;
        }
    }
}