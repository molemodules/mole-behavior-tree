using System;

namespace MoleGames.BehaviorTree
{
    public abstract class ConditionBTN : BehaviorTreeNode
    {
        readonly BehaviorTreeNode child;

        abstract protected Func<bool> Predicate { get; }

        protected ConditionBTN(BehaviorTreeNode child)
        {
            this.child = child;
        }

        public sealed override ExecutableNodeState Evaluate()
        {
            return Predicate() ? child.Evaluate() : ExecutableNodeState.Failed;
        }
    }
}