using System.Collections;
using UnityEngine;

namespace MoleGames.BehaviorTree
{
    public abstract class BehaviorTree : MonoBehaviour
    {
        [SerializeField, Range(0, 1)] float period;
        bool running;

        abstract protected BehaviorTreeNode Tree { get; }

        public void Enable()
        {
            running = true;
            StartCoroutine(Tick());
        }

        public void Disable()
        {
            StopAllCoroutines();
            running = false;
        }

        IEnumerator Tick()
        {
            while (running)
            {
                Tree.Evaluate();
                yield return new WaitForSeconds(period);
            }

            yield break;
        }
    }
}
